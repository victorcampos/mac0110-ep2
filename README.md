# MAC0110-EP2

EP2 coursework for MAC0110 discipline

Exercise 1 requires us to create a function, checkWill(cards), which
receives a given array, and attempts to sort the array in order to
put the numbers 6,1,7,4 in order to form the Kaprekar constant. If
it's possible, the function shall return a Boolean true, and if it's
not, it'll return a Boolean false.
The other exercises similarly employ a given array with a number set
and return a boolean value on whether it's possible to find that number
within the set or not.
