# ***Teria sido mais simples assistir a aula antes e ter feito a checagem considerando a interseção de dois conjuntos - ops 
# versão de garantia sem exercício bônus

#pra ser possível formar o número 6174, basta existir seus 4 algarismos dentro de um array
function checkWill(cards)
    if length(cards) < 4
        return false
    elseif length(cards) >= 4
        resp = []
        for pc in 1:(length(cards))
            if cards[pc] == 6
                push!(resp, cards[pc])
                break
            end
            pc += 1
        end
        if resp == [6]
            for pc in 1:length(cards)
                if cards[pc] == 1
                    push!(resp, cards[pc])
                    break
                end
                pc = pc + 1
            end
            if resp == [6,1]
                pc = 1
                for pc in 1:length(cards)
                    if cards[pc] == 7
                        push!(resp, cards[pc])
                        break
                    end
                    pc = pc + 1
                end
                if resp == [6,1,7]
                    pc = 1
                    for pc in 1:length(cards)
                        if cards[pc] == 4
                            return true
                        end
                    end
                end
            end
        end
    return false
    end
end

# Para formar 6174 e 7711, conforme o exemplo apresentado no enunciado, precisamos
# necessariamente de três 7s, três 1s, um 6 e um 4. A função apagará os elementos
# após utilizar-los.

function checkTaki(cards)
    if length(cards) < 8
        return false
    elseif length(cards) >= 8
        resp = []
        for pc in 1:(length(cards))
            if cards[pc] == 6
                push!(resp, cards[pc])
                deleteat!(cards,pc)
                break # break tem a função de nos tirar do loop, já que não precisamos mais dele
            end
            pc = pc + 1
        end
        if resp == [6]
            for pc in 1:length(cards)
                if cards[pc] == 1
                    push!(resp, cards[pc])
                    deleteat!(cards,pc)
                    break
                end
                pc = pc + 1
            end
            if resp == [6,1]
                pc = 1
                for pc in 1:length(cards)
                    if cards[pc] == 7
                        push!(resp, cards[pc])
                        deleteat!(cards,pc)
                        break
                    end
                    pc = pc + 1
                end
                if resp == [6,1,7]
                    pc = 1
                    for pc in 1:length(cards)
                        if cards[pc] == 4
                            push!(resp,cards[pc])
                            deleteat!(cards,pc)
                            break
                        end
                    pc = pc + 1
                    end
                    if resp == [6,1,7,4]
                        pc = 1
                        for pc in 1:length(cards)
                            if cards[pc] == 7
                                push!(resp,cards[pc])
                                deleteat!(cards,pc)
                                break
                            end
                        pc = pc + 1
                        end
                        if resp == [6,1,7,4,7]
                            pc = 1
                            for pc in 1:length(cards)
                                if cards[pc] == 7
                                    push!(resp,cards[pc])
                                    deleteat!(cards,pc)
                                    break
                                end
                            pc = pc + 1
                            end
                            if resp == [6,1,7,4,7,7]
                                pc = 1
                                for pc in 1:length(cards)
                                    if cards[pc] == 1
                                        push!(resp,cards[pc])
                                        deleteat!(cards,pc)
                                        break
                                    end
                                pc = pc + 1
                                end
                                if resp == [6,1,7,4,7,7,1]
                                    pc = 1
                                    for pc in 1:length(cards)
                                        if cards[pc] == 1
                                            return true
                                        end
                                    pc = pc + 1
                                    end
                                end
                            end
                        end
                    end
                end
            end
        end
    return false
    end
end

function checkJackson(x,cards)
    if length(cards) < 8
        return false
    elseif length(cards) >= 8
        resp = []
        for pc in 1:(length(cards))
            if cards[pc] == 6
                push!(resp, cards[pc])
                deleteat!(cards,pc)
                break
            end
            pc = pc + 1
        end
        if resp == [6]
            for pc in 1:length(cards)
                if cards[pc] == 1
                    push!(resp, cards[pc])
                    deleteat!(cards,pc)
                    break
                end
                pc = pc + 1
            end
            if resp == [6,1]
                pc = 1
                for pc in 1:length(cards)
                    if cards[pc] == 7
                        push!(resp, cards[pc])
                        deleteat!(cards,pc)
                        break
                    end
                    pc = pc + 1
                end
                if resp == [6,1,7]
                    pc = 1
                    for pc in 1:length(cards)
                        if cards[pc] == 4
                            push!(resp,cards[pc])
                            deleteat!(cards,pc)
                            break
                        end
                    pc = pc + 1
                    end
                    if resp == [6,1,7,4]
                        pc = 1
                        for pc in 1:length(cards)
                            if cards[pc] == 7
                                push!(resp,cards[pc])
                                deleteat!(cards,pc)
                                break
                            end
                        pc = pc + 1
                        end
                        if resp == [6,1,7,4,7]
                            pc = 1
                            for pc in 1:length(cards)
                                if cards[pc] == 7
                                    push!(resp,cards[pc])
                                    deleteat!(cards,pc)
                                    break
                                end
                            pc = pc + 1
                            end
                            if resp == [6,1,7,4,7,7]
                                pc = 1
                                for pc in 1:length(cards)
                                    if cards[pc] == 1
                                        push!(resp,cards[pc])
                                        deleteat!(cards,pc)
                                        break
                                    end
                                pc = pc + 1
                                end
                                if resp == [6,1,7,4,7,7,1]
                                    pc = 1
                                    for pc in 1:length(cards)
                                        if cards[pc] == 1
                                            push!(resp,cards[pc])
                                            resp = pushfirst!(resp, x)
                                            if resp == [x,6,1,7,4,7,7,1,1]
                                                return true
                                            end   
                                        end
                                    pc = pc + 1
                                    end
                                end
                            end
                        end
                    end
                end
            end
        end
    return false
    end
end

function checkWillBase(b, cards) # Poderia ter visto a aula de ordenação também antes
    if b > 10 || b < 2           # de ter feito pra ficar mais bonito - ops de novo
        return false
    else # vamos primeiro converter 6174 da base 10 pra base b
        soma = 0
        pot = 1
        ref = 6174
        while ref > 0
            dig = ref % b
            soma = soma + dig * pot
            ref = ref ÷ b
            pot = pot * 10
        end
        # salvamos soma num vetor e o reorganizamos - digits teria a opção de mudança de base tb
        arr = digits(soma)
        for i in 1:length(arr) #Vamos fazer um selection sort pra garantir ordenação de arr
            min = arr[i]
            ind = i
            for j in (i+1):length(arr)
                if arr[j] < arr[ind]
                    ind = j
                end
            end
            arr[i], arr[ind] = arr[ind], arr[i]
        end        
        for i in 1:length(cards) # E igualmente para o array cards
            min = cards[i]
            ind = i
            for j in (i+1):length(cards)
                if cards[j] < cards[ind]
                    ind = j
                end
            end
            cards[i], cards[ind] = cards[ind], cards[i]
        end        
        parr = 1 # ponteiro em arr
        pcards = 1 # ponteiro em cards
        resp = []
        while parr <= length(arr) && pcards <= length(cards) # procurando intersecao entre os arrays
            if arr[parr] < cards[pcards]
                parr += 1
            elseif cards[pcards] < arr[parr]
                pcards += 1
            else
                push!(resp, cards[pcards])
                parr += 1
                pcards += 1
            end
        end
        for i in 1:length(resp) # após fazer a interseção, vamos fazer um um sort no array de resposta resp.
            min = resp[i]
            ind = i
            for j in (i+1):length(resp)
                if resp[j] < resp[ind]
                    ind = j
                end
            end
            resp[i], resp[ind] = resp[ind], resp[i]
        end
        if resp == arr
            return true
        end
    end
    return false
end

# Exercício Bônus - CheckJackson com Complemento do conjunto

function checkFriends(numbers, cards)
    #colocamos o 6174 em numbers
    pushfirst!(numbers, 6174)
    # primeiro varreremos o array em numbers por todos os seus digitos
    tamNumbers = length(numbers)
    arrNumbers = []
    for i in 1:tamNumbers
        dig = digits(numbers[i])
        push!(arrNumbers,dig)
        i += 1
    end
    # Varreremos cada linha de arrNumbers por elementos iguais em cards
    tamCards = length(cards)
    for j in 1:length(arrNumbers)
        for i in 1:length(arrNumbers[j])
            if cards == []
                return nothing
            end
            for k in 1:tamCards
                if arrNumbers[j][i] == cards[k]
                    cards = deleteat!(cards, k)
                    break
                elseif findfirst(isequal(arrNumbers[j][i]),cards) === nothing # não soube fazer isso sem o findfirst xd
                    return nothing
                else
                    k += 1
                end
            end
            i += 1
        end
        j += 1
    end
    return cards 
end
# *** TESTES AUTOMATIZADOS ***

using Test
println("Testando as funcoes... ")
print("checkWill...      ")
@test checkWill([0,1]) == false
@test checkWill([1,4,6,7]) == true
@test checkWill([0,1,2,3,4,5,6,7,8,9]) == true
@test checkWill([6,6,1,1,4,4,7,7]) == true
println("OK!")

print("checkTaki...      ")
@test checkTaki([1,4,6,7]) == false
@test checkTaki([0,1,2,3,4,5,6,7,8,9]) == false
@test checkTaki([6,1,4,7,7,7,1,1]) == true
@test checkTaki([6,6,1,4,4,7,7,7,7,1,1,2]) == true
@test checkTaki([7,1,7,1,7,1,6,4]) == true
println("OK!")

print("checkJackson...   ")
@test checkJackson(12,[1,4,6,7]) == false
@test checkJackson(32,[0,1,2,3,4,5,6,7,8,9]) == false
@test checkJackson(6,[1,4,7,7,7,1,1]) == false
@test checkJackson(986,[6,1,4,7,7,7,1,1]) == true
@test checkJackson(9018,[6,6,1,4,4,7,7,7,7,1,1,2]) == true
@test checkJackson(87,[7,1,7,1,7,1,6,4]) == true
println("OK!")

print("checkWillBase...  ")
@test checkWillBase(12,[1,4,6,7]) == false
@test checkWillBase(9,[0,2,8,4]) == true
@test checkWillBase(6,[4,4,3,3,0]) == true
@test checkWillBase(2,[1, 1, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0]) == true
@test checkWillBase(7,[6,6,1,4,4,7,7,7,7,1,1,2]) == false
@test checkWillBase(4,[1,2,0,0,1,3,2]) == true
@test checkWillBase(8, [1, 4, 0, 3, 6, 9, 0]) == true
println("OK!")

print("checkFriends...   ")
@test checkFriends([123,456],[6,1,7,4,1,2,3,4,5,6]) == []
@test checkFriends([8, 100], [6, 1, 7, 4]) === nothing
@test checkFriends([2], [2]) === nothing
@test checkFriends([32,44], [6,1,7,4,3,2,4,4,3,2]) == [3,2]
@test checkFriends([24, 42, 6174], [2, 4, 4, 2, 6, 1, 7, 4, 6, 1, 7, 4]) == []
println("OK!")

println(" ")
printstyled("Todos os testes obtiveram sucesso!", bold = true, color = :green)
